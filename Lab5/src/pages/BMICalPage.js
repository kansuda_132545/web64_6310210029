
import BMIResult from "../components/ฺBMIResult";
import { useState } from "react";
import Button from '@mui/material/Button';
import  Grid  from "@mui/material/Grid";
import { Typography, Box, Container } from "@mui/material" ;

function BMICalPage() {

    const [ name , setName] = useState();
    const [ bmiResult , setBmiresult] = useState(0);
    const [ translateResult , setTranslateResult] = useState(0);

    const [ height , setHeight] = useState("");
    const [ weight , setWeight] = useState("");

   function calculateBMI () {
       let h = parseFloat(height);
       let w = parseFloat(weight)
       let bmi = ( w / ( h * h ) );
       setBmiresult ( bmi );
       if (bmi > 25){
           setTranslateResult ("เริ่มอ้วนแล้วนะ");
       }else {
           setTranslateResult ("สมส่วน")
       }
   }
        
    return(

       <Container maxWidth = 'lg'> 
        <Grid container spacing={2} sx= {{ marginTop : "10px"}}>
            <Grid item xs={12}>
               <Typography variant="h5">
               ยินดีต้อนรับสู่เว็บคำนวณ BMI
                   </Typography>
            </Grid>
            <Grid item xs={8} >
                <Box sx={{ textAlign : 'left'}}>

                
                คุณชื่อ : <input type="text" 
                            value={name}
                            onChange={
                                (e) => {
                                setName (e.target.value);
                            }
                            } /> <br/>
                            <br/>
                ส่วนสูง : <input type="text"value={height}
                            onChange={
                                (e) => {
                                setHeight (e.target.value);
                            }
                            } /> <br/>
                            <br/>
                น้ำหนัก : <input type="text"value={weight}
                            onChange={
                                (e) => {
                                setWeight (e.target.value);
                            }
                            } /> <br/>
                            <br/>
               
               
                <Button variant="contained"  onClick={ () =>{ calculateBMI( ) } } >Calculate</Button>
                </Box>
            </Grid>
            <Grid item xs={4}>
                    { bmiResult != 0 &&
                        
                        <div>
                        <hr/>
                            ผลการคำนวณ

                            <BMIResult
                            name= {name}   
                            bmi = { bmiResult}
                            result = {translateResult} 
                            />
                </div>
                    }
            </Grid>
        </Grid>
        </Container>


    );
}export default BMICalPage



/*<div align = "left">
            <div align = " center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr/>
                
                คุณชื่อ : <input type="text" 
                            value={name}
                            onChange={
                                (e) => {
                                setName (e.target.value);
                            }
                            } /> <br/>
                ส่วนสูง : <input type="text"value={height}
                            onChange={
                                (e) => {
                                setHeight (e.target.value);
                            }
                            } /> <br/>
                น้ำหนัก : <input type="text"value={weight}
                            onChange={
                                (e) => {
                                setWeight (e.target.value);
                            }
                            } /> <br/>

               
                <Button variant="contained"  onClick={ () =>{ calculateBMI( ) } } >Calculate</Button>
                
                
                
                { bmiResult != 0 &&
                
                <div>
                <hr/>
                    ผลการคำนวณ

                    <BMIResult
                    name= {name}   
                    bmi = { bmiResult}
                    result = {translateResult} 
                    />
                </div>
                    }
                </div> 


        </div>*/